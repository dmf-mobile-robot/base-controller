#include <ros.h>
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Vector3Stamped.h"

#include <SoftwareSerial.h>
#include <MeSmartServo.h>
#include <MeOrion.h>

//initializing all the variables
#define LOOPTIME 100  //Looptime in millisecond
#define CORRECTION_FACTOR 1.038

unsigned long lastMilli = 0;
bool orange = false;
int cmd_velMessageRecieved = 0;
ros::Time last_time;

MeSmartServo mysmartservo(PORT_5);  //UART2 is on port 5

ros::NodeHandle n;

void messageCb(const geometry_msgs::Twist& vel) {
  cmd_velMessageRecieved = 0;

  float velX = vel.linear.x * 300;
  float rotT = vel.angular.z * 300;

  float rightWheelSpeed = velX + rotT / 2;
  float leftWheelSpeed = velX - rotT / 2;

  leftWheelSpeed *= CORRECTION_FACTOR;

  mysmartservo.setPwmMove(1, -rightWheelSpeed);
  mysmartservo.setPwmMove(2, leftWheelSpeed);
}

ros::Subscriber<geometry_msgs::Twist> subCmdVel("cmd_vel", messageCb);

geometry_msgs::Vector3Stamped jointStates;
ros::Publisher pubWheelJointStates("wheel_joint_states", &jointStates);

void setup() {
  n.initNode();
  n.subscribe(subCmdVel);
  n.advertise(pubWheelJointStates);

  mysmartservo.begin(115200);
  delay(5);
  mysmartservo.assignDevIdRequest();
  mysmartservo.setRGBLed(1, 255, 0, 0);
  mysmartservo.setRGBLed(2, 255, 0, 0);

  mysmartservo.setZero(1);
  mysmartservo.setZero(2);
  delay(1000);

  mysmartservo.setBreak(1, 1);
  mysmartservo.setBreak(2, 1);

  last_time = n.now();
}

void loop() {

  n.spinOnce();

  if (millis() - lastMilli >= LOOPTIME && checkConnectionToMaster()) {
    blinkSmarServeLED();

    publishJointStates(LOOPTIME);

    lastMilli = millis();
  }
}

void blinkSmarServeLED() {
  if (orange) {
    orange = false;
    mysmartservo.setRGBLed(1, 255, 0, 255);
    mysmartservo.setRGBLed(2, 255, 255, 0);
  } else {
    orange = true;
    mysmartservo.setRGBLed(1, 255, 255, 0);
    mysmartservo.setRGBLed(2, 255, 0, 255);
  }
}

void publishJointStates(double time) {
  jointStates.header.stamp = n.now();
  jointStates.vector.x = -mysmartservo.getAngleRequest(1);
  jointStates.vector.y = mysmartservo.getAngleRequest(2);
  jointStates.vector.z = (n.now() - last_time).toSec();
  pubWheelJointStates.publish(&jointStates);
  last_time = n.now();
  n.spinOnce();
  //n.loginfo("published wheel joint states");
}

bool checkConnectionToMaster() {

  if (cmd_velMessageRecieved > 0) {
    mysmartservo.setRGBLed(1, 255, 0, 0);
    mysmartservo.setRGBLed(2, 255, 0, 0);
    mysmartservo.setPwmMove(1, 0);
    mysmartservo.setPwmMove(2, 0);
    return false;
  } else {
    cmd_velMessageRecieved++;
    return true;
  }
}
